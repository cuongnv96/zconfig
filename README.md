# Block typing and seen

Block typing and seen on Zalo Web:

1. Install Adblock Plus on your Chrome browser.
2. Put urls below in your filter:
```
https://chat*-wpa.api.zalo.me/api/message/typing
https://group-wpa.zalo.me/api/group/typing
https://chat*-wpa.api.zalo.me/api/message/seen
https://group-wpa.zalo.me/api/group/seen
```
3. Save and refresh current Zalo Tab.

# Block recall message

Please pull code and contribute to me what I can do to recall message :)